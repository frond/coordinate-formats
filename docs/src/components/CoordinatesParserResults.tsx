import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import MyLocationIcon from '@mui/icons-material/MyLocation';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Snackbar from '@mui/material/Snackbar';
import * as A from 'fp-ts/lib/Array';
import { pipe } from 'fp-ts/lib/function';
import React, { useRef, useState } from 'react';
import {
  CoordinatesParserResult,
  coordinatesParserSuccessToString,
  isCoordinatesParserSuccess,
} from '../../../src';

export const CoordinatesParserResultItems = ({
  parserResults,
  selectedResult,
  setSelectedResult,
}: {
  parserResults: ReadonlyArray<CoordinatesParserResult>;
  selectedResult: number | undefined;
  setSelectedResult: React.Dispatch<React.SetStateAction<number | undefined>>;
}) => {
  const [openedResult, setOpenedResult] = useState<number | undefined>(
    undefined
  );
  return (
    <List sx={{ width: '100%' }} dense>
      {parserResults.map((parserResult, index) => (
        <CoordinatesParserResultItem
          index={index}
          key={`parser-result-${index}`}
          parserResult={parserResult}
          selectedResult={selectedResult}
          setSelectedResult={setSelectedResult}
          openedResult={openedResult}
          setOpenedResult={setOpenedResult}
        />
      ))}
    </List>
  );
};

const CoordinatesParserResultItem = ({
  parserResult,
  selectedResult,
  setSelectedResult,
  openedResult,
  setOpenedResult,
  index,
}: {
  index: number;
  parserResult: CoordinatesParserResult;
  selectedResult: number | undefined;
  setSelectedResult: React.Dispatch<React.SetStateAction<number | undefined>>;
  openedResult: number | undefined;
  setOpenedResult: React.Dispatch<React.SetStateAction<number | undefined>>;
}) => {
  const [isOpenSnackbar, setIsOpenSnackbar] = useState(false);
  const refCoordinatesString = useRef<HTMLInputElement | undefined>(undefined);

  const hash = isCoordinatesParserSuccess(parserResult)
    ? pipe(
        parserResult.latLon,
        A.reduce(0, (a, b) => a + b)
      )
    : index;

  const open = openedResult === hash;

  return (
    <>
      {parserResult._tag === 'ParserError' ? (
        <ListItem
          sx={{
            border: '2px solid transparent',
          }}
          dense
        >
          <ListItemAvatar>
            <Avatar sx={{ backgroundColor: 'red' }}>
              <ErrorOutlineIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={parserResult.errors.join(' ')}
            secondary={parserResult.format}
          />
          <IconButton
            onClick={() => {
              setOpenedResult(open ? undefined : hash);
            }}
            size="small"
          >
            {open ? (
              <ExpandLess fontSize="small" />
            ) : (
              <ExpandMore fontSize="small" />
            )}
          </IconButton>
        </ListItem>
      ) : null}
      {parserResult._tag === 'ParserValidationError' ? (
        <ListItem
          sx={{
            border: '2px solid transparent',
          }}
        >
          <ListItemAvatar>
            <Avatar sx={{ backgroundColor: 'orange' }}>
              <WarningAmberIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={parserResult.errors.join(' ')}
            secondary={parserResult.format}
          />
          <IconButton
            onClick={() => {
              setOpenedResult(open ? undefined : hash);
            }}
            size="small"
          >
            {open ? (
              <ExpandLess fontSize="small" />
            ) : (
              <ExpandMore fontSize="small" />
            )}
          </IconButton>
        </ListItem>
      ) : null}
      {parserResult._tag === 'ParserSuccess' ? (
        <ListItem
          sx={{
            border: '2px solid transparent',
            ...(hash === selectedResult
              ? { border: '2px solid green', borderRadius: '4px' }
              : {}),
          }}
        >
          <ListItemAvatar>
            <Avatar sx={{ backgroundColor: 'green' }}>
              <CheckCircleIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={
              <InputBase
                inputRef={refCoordinatesString}
                value={coordinatesParserSuccessToString(parserResult)}
                readOnly
                sx={{ width: '100%' }}
              />
            }
            secondary={parserResult.format}
          />
          <IconButton
            onClick={() => {
              setSelectedResult(selectedResult === hash ? undefined : hash);
            }}
            size="small"
          >
            {selectedResult === hash ? (
              <MyLocationIcon fontSize="small" />
            ) : (
              <LocationSearchingIcon fontSize="small" />
            )}
          </IconButton>
          <IconButton
            onClick={() => {
              const input = refCoordinatesString.current;
              if (input) {
                input.select();
                document.execCommand('copy');
                setIsOpenSnackbar(true);
              }
            }}
            size="small"
          >
            <ContentCopyIcon fontSize="small" />
          </IconButton>
          <Snackbar
            open={isOpenSnackbar}
            onClose={() => setIsOpenSnackbar(false)}
            message={`Successfully copied «${coordinatesParserSuccessToString(
              parserResult
            )}» to clipboard.`}
          />
          <IconButton
            onClick={() => {
              setOpenedResult(open ? undefined : hash);
            }}
            size="small"
          >
            {open ? (
              <ExpandLess fontSize="small" />
            ) : (
              <ExpandMore fontSize="small" />
            )}
          </IconButton>
        </ListItem>
      ) : null}
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Box
          component="pre"
          sx={{
            overflowX: 'auto',
          }}
        >
          <pre>{JSON.stringify(parserResult, null, 2)}</pre>
        </Box>
      </Collapse>
    </>
  );
};
