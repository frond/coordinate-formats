import { fixtures } from '../../../src';
import React from 'react';

export const SupportedFormats = () => {
  return (
    <ul>
      {fixtures.map(({ query, expected }) => (
        <li key={query} style={{ marginBottom: '1rem' }}>
          <code>{query}</code>
          <ul>
            {expected.map((val) => (
              <li key={`${query}-${val}`}>
                <code>{val}</code>
              </li>
            ))}
          </ul>
        </li>
      ))}
    </ul>
  );
};
