import BrowserOnly from '@docusaurus/BrowserOnly';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import SyncIcon from '@mui/icons-material/Sync';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import OutlinedInput from '@mui/material/OutlinedInput';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import * as A from 'fp-ts/lib/Array';
import { flow, pipe } from 'fp-ts/lib/function';
import * as I from 'fp-ts/lib/Identity';
import { not } from 'fp-ts/lib/Predicate';
import React, { useEffect, useState } from 'react';
import {
  CoordinatesParserResult,
  coordinatesParserSuccessToString,
  DefaultFormatKey,
  defaultFormatKeys,
  isCoordinatesParserError,
  isCoordinatesParserSuccess,
  isCoordinatesParserValidationError,
  parseCoordinates,
  fixtures,
} from '../../../src';
import { CoordinatesParserResultItems } from './CoordinatesParserResults';
import { PositionsMap } from './PositionsMap';
import { useColorMode } from '@docusaurus/theme-common';
import { MenuItem, Select } from '@mui/material';

export const ParserDemo = () => {
  const getRandomQuery = () =>
    fixtures[Math.floor(Math.random() * fixtures.length)].query;

  const [query, setQuery] = useState(getRandomQuery());
  const [results, setResults] = useState<
    ReadonlyArray<CoordinatesParserResult>
  >([]);
  const [selectedFormatKey, setSelectedFormatKey] = useState(
    'all' as DefaultFormatKey | 'all'
  );
  const [isShownValidationErrors, setIsShownValidationErrors] = useState(false);
  const [isShownParserErrors, setIsShownParserErrors] = useState(false);
  const [selectedResult, setSelectedResult] = useState<number | undefined>(
    undefined
  );
  const { colorMode } = useColorMode();

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode: colorMode,
        },
      }),
    [colorMode]
  );

  useEffect(() => {
    const results = parseCoordinates(query);
    setResults(results);
  }, [query]);

  const loadRandomFixture = () => {
    setQuery(getRandomQuery());
  };

  const filteredResults = pipe(
    results,
    (a) =>
      selectedFormatKey === 'all'
        ? a
        : a.filter((b) => b.format === selectedFormatKey),
    isShownParserErrors ? I.of : flow(A.filter(not(isCoordinatesParserError))),
    isShownValidationErrors
      ? I.of
      : flow(A.filter(not(isCoordinatesParserValidationError)))
  );

  const labeledLatLons = pipe(
    filteredResults,
    A.filter(isCoordinatesParserSuccess),
    A.map((x) => ({
      label: coordinatesParserSuccessToString(x),
      latLon: x.latLon,
      result: x,
    }))
  );

  return (
    <ThemeProvider theme={theme}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          marginBottom: '1rem',
          maxWidth: '100%',
        }}
      >
        <OutlinedInput
          sx={{ flexGrow: 1 }}
          placeholder="Enter a coordinates string to parse"
          inputProps={{
            'aria-label': 'enter a coordinates string to parse',
          }}
          value={query}
          onChange={(event) => setQuery(event.target.value)}
          startAdornment={
            <InputAdornment position="start">
              <IconButton onClick={loadRandomFixture} aria-label="menu">
                <SyncIcon />
              </IconButton>
            </InputAdornment>
          }
        />
      </Box>
      <Box>
        <Select
          value={selectedFormatKey}
          onChange={(e) =>
            setSelectedFormatKey(e.target.value as DefaultFormatKey | 'all')
          }
          size="small"
          sx={{ marginBottom: '0.5rem' }}
        >
          <MenuItem value={'all'} key="allFormats">
            All Formats
          </MenuItem>
          {defaultFormatKeys.map((key) => (
            <MenuItem value={key} key={key}>
              {key}
            </MenuItem>
          ))}
        </Select>
      </Box>
      <Box>
        <Grid container columnSpacing={2}>
          <Grid
            item
            xs={12}
            sx={{
              display: 'flex',
              flexDirection: 'column',
              maxHeight: '300px',
            }}
          >
            <Box sx={{ marginBottom: '0.5rem' }}>
              <Button
                onClick={() =>
                  setIsShownValidationErrors(!isShownValidationErrors)
                }
                startIcon={
                  isShownValidationErrors ? (
                    <CheckBoxIcon />
                  ) : (
                    <CheckBoxOutlineBlankIcon />
                  )
                }
              >
                Validation errors
              </Button>
              <Button
                onClick={() => setIsShownParserErrors(!isShownParserErrors)}
                startIcon={
                  isShownParserErrors ? (
                    <CheckBoxIcon />
                  ) : (
                    <CheckBoxOutlineBlankIcon />
                  )
                }
              >
                Parsing errors
              </Button>
            </Box>
            <Box sx={{ flexGrow: 1, overflowY: 'auto' }}>
              {filteredResults.length ? (
                <CoordinatesParserResultItems
                  parserResults={filteredResults}
                  selectedResult={selectedResult}
                  setSelectedResult={setSelectedResult}
                />
              ) : (
                <Box sx={{ fontStyle: 'italic', paddingY: '1rem' }}>
                  No Results
                </Box>
              )}
            </Box>
          </Grid>
          <Grid item xs={12}>
            <BrowserOnly>
              {() => (
                <PositionsMap
                  labeledLatLons={labeledLatLons}
                  selectedResult={selectedResult}
                  setSelectedResult={setSelectedResult}
                />
              )}
            </BrowserOnly>
          </Grid>
        </Grid>
      </Box>
    </ThemeProvider>
  );
};
