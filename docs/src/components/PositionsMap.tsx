import React, { useEffect } from 'react';
import { LatLonTuple } from '../../../src';
import 'leaflet/dist/leaflet.css';
import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { grey } from '@mui/material/colors';

type LabeledLatLon = {
  label: string;
  latLon: LatLonTuple;
};

type PositionsMapMarkersProps = {
  labeledLatLons: Array<LabeledLatLon>;
  selectedResult: number | undefined;
  setSelectedResult: React.Dispatch<React.SetStateAction<number | undefined>>;
};

type PositionsMapProps = PositionsMapMarkersProps;

export const PositionsMap = (props: PositionsMapProps) => {
  const MapContainer = require('react-leaflet').MapContainer;
  const TileLayer = require('react-leaflet').TileLayer;
  const Polyline = require('react-leaflet').Polyline;

  return (
    <MapContainer
      center={[51.505, -0.09]}
      zoom={5}
      style={{ height: '500px' }}
      scrollWheelZoom={false}
    >
      <TileLayer
        url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZnJvbmQiLCJhIjoiY2thcmJ3bGtjMDhtNzJ3bnFqcnU1NDRhMCJ9.qVxb-z8gEdxvpGBcFvCBuQ`}
        tileSize={512}
        zoomOffset={-1}
        attribution='© <a href="https://www.mapbox.com/contribute/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      />
      <Polyline
        positions={[
          [-85, 0],
          [85, 0],
        ]}
        pathOptions={{ color: grey['500'], dashArray: [8], weight: 2 }}
      />
      <Polyline
        positions={[
          [0, -1000],
          [0, 1000],
        ]}
        pathOptions={{ color: grey['500'], dashArray: [8], weight: 2 }}
      />
      <PositionsMapMarkers {...props} />
    </MapContainer>
  );
};

const PositionsMapMarkers = ({
  labeledLatLons,
  selectedResult,
  setSelectedResult,
}: PositionsMapMarkersProps) => {
  const L = require('leaflet');
  const useMap = require('react-leaflet').useMap;

  const map = useMap();

  useEffect(() => {
    if (labeledLatLons.length) {
      map.fitBounds(
        pipe(
          labeledLatLons,
          A.map(({ latLon }) => latLon),
          L.latLngBounds
        ),
        {
          maxZoom: 7,
        }
      );
    }
  }, [labeledLatLons]);

  return (
    <>
      {labeledLatLons.map((labeledLatLon, index) => (
        <PositionsMapMarker
          labeledLatLon={labeledLatLon}
          selectedResult={selectedResult}
          setSelectedResult={setSelectedResult}
          key={`map-position-${index}`}
        />
      ))}
    </>
  );
};

const PositionsMapMarker = ({
  labeledLatLon: { latLon, label },
  selectedResult,
  setSelectedResult,
}: {
  labeledLatLon: LabeledLatLon;
  selectedResult: number | undefined;
  setSelectedResult: React.Dispatch<React.SetStateAction<number | undefined>>;
}) => {
  const CircleMarker = require('react-leaflet').CircleMarker;
  const Tooltip = require('react-leaflet').Tooltip;

  const hash = latLon.reduce((a, b) => a + b);
  return (
    <CircleMarker
      center={latLon}
      pathOptions={{
        color: 'green',
        ...(selectedResult === hash
          ? { weight: 2, fillOpacity: 1 }
          : { weight: 0, fillOpacity: 0.25 }),
      }}
      eventHandlers={{
        click: () => {
          setSelectedResult(hash);
        },
      }}
    >
      <Tooltip direction="top">{label}</Tooltip>
    </CircleMarker>
  );
};
