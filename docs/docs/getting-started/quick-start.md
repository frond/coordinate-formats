# Quick Start

## Parse Coordinates
Parse coordinates using the default formats and options:
```ts
import { parseCoordinates } from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E');
```

## Filter by Result Type
This will give you a list of results, where every item can of the following type:
- `CoordinatesParserError`: An error when trying to parse the coordinates.
- `CoordinatesParserValidationError`: A validation error (i.e. the longitude degrees are greater than 180).
- `CoordinatesParserSuccess`: Successfully parsed coordinates.

For all of these types of results, `coordinate-formats` also provides useful filter predicates. So in order to get all the successfully
parsed coordinates, you can use the `isCoordinatesParserSuccess` predicate:

```ts
import {
  parseCoordinates,
  // highlight-next-line
  isCoordinatesParserSuccess
} from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E')
  // highlight-next-line
  .filter(isCoordinatesParserSuccess)
```

## Filter by Format
This will give you a list of all successfully parsed coordinates. However for the given coordinates string `27.15.45S 18.32.53E` this will
provide two results in different formats:
- DD (Degrees Minutes): `S 27° 15.45', E 018° 32.53'`
- DMS (Degrees Minutes Seconds): `S 27° 15' 45", E 018° 32' 53"`

So what if we're after a specific format? `coordinate-formats` also exposes filter predicates for every format:

```ts
import {
  parseCoordinates,
  isCoordinatesParserSuccess,
  // highlight-next-line
  DD
} from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E')
  .filter(isCoordinatesParserSuccess)
  // highlight-next-line
  .filter(DD.isFormatOfResult)
```

And to make it even more straight forward, you can also use the format's `isCoordinatesParserSuccess` predicate to achieve the same result:

```ts
import {
  parseCoordinates,
  DD
} from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E')
  // highlight-next-line
  .filter(DD.isCoordinatesParserSuccess)
```

## Using Parsed Coordinates in Maps
All successfully parsed coordinates are converted to latitude/longitude decimal degrees. So to integrate them into your map,
you can use the `latLon` property:

```ts
import {
  parseCoordinates,
  DD
} from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E')
  .filter(DD.isCoordinatesParserSuccess)
  // highlight-next-line
  .map((res) => res.latLon)
```

## Formatting Parsed Coordinates as Strings
You can also format the parsed coordinates as strings, i.e. when building an UI:

```ts
import {
  parseCoordinates,
  DD,
  // highlight-next-line
  coordinatesParserSuccessToString
} from 'coordinate-formats';

const results = parseCoordinates('27.15.45S 18.32.53E')
  .filter(DD.isCoordinatesParserSuccess)
  // highlight-next-line
  .map(coordinatesParserSuccessToString)
```
