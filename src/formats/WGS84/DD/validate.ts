import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import { makeValidateCoordinates } from '../../../constructors';
import { CoordinatesDD } from './convert';
import { validateDegrees } from '../validations';
import { CoordinatePartsDD, ParserOptionsDD } from './parse';
import {
  containCharactersConsistently,
  containsOnlyValidCharacters,
} from '../../../validations';

export const validateCoordinatePartsDD = (
  parts: CoordinatePartsDD,
  options: ParserOptionsDD
) =>
  pipe(
    [
      containsOnlyValidCharacters({
        key: 'prefix',
        str: parts.prefix,
        validCharacters: [
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'middle',
        str: parts.middle,
        validCharacters: [
          ...options.symbols.degrees,
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.symbols.whitespace,
          ...options.separators.coordinates,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'suffix',
        str: parts.suffix,
        validCharacters: [
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.degrees,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latDegreesBetween, parts.lonDegreesBetween],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
    ],
    A.filterMap(O.fromNullable)
  );

export const validateCoordinatesDD = makeValidateCoordinates(
  ({ latitude, longitude }: CoordinatesDD) =>
    pipe(
      [
        { key: 'latitude', coordinate: latitude },
        { key: 'longitude', coordinate: longitude },
      ],
      A.chain(({ key, coordinate }) => [
        validateDegrees({ key, degrees: coordinate.degrees }),
      ]),
      A.filterMap(O.fromNullable)
    )
);
