import { makeCoordinatesFormat } from '../../../constructors';
import { formatDD } from './constants';
import { latLonToCoordinatesDD, coordinatesToLatLonDD } from './convert';
import {
  defaultStringifyOptionsDD,
  makeCoordinatesToStringDD,
} from './stringify';
import { validateCoordinatesDD } from './validate';
import { defaultParserOptionsDD, makeParseCoordinatesDD } from './parse';

/**
 * @category Core
 */
export const DD = makeCoordinatesFormat({
  format: formatDD,
  coordinatesToLatLon: coordinatesToLatLonDD,
  latLonToCoordinates: latLonToCoordinatesDD,
  defaultStringifyOptions: defaultStringifyOptionsDD,
  makeCoordinatesToString: makeCoordinatesToStringDD,
  validateCoordinates: validateCoordinatesDD,
  defaultParserOptions: defaultParserOptionsDD,
  makeParseCoordinates: makeParseCoordinatesDD,
});
