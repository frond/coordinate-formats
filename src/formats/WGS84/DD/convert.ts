import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import {
  makeCoordinatesToLatLon,
  makeLatLonToCoordinates,
} from '../../../constructors';
import { LatLonTuple } from '../../../types';

export const latLonToCoordinatesDD = makeLatLonToCoordinates((x) => ({
  latitude: {
    degrees: x[0],
  },
  longitude: {
    degrees: x[1],
  },
}));

export type CoordinatesDD = ReturnType<typeof latLonToCoordinatesDD>;

export const coordinatesToLatLonDD = makeCoordinatesToLatLon(
  ({ latitude, longitude }: CoordinatesDD) =>
    pipe(
      [latitude, longitude],
      A.map((x) => x.degrees),
      (x) => x as LatLonTuple
    )
);
