import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import { flow, pipe } from 'fp-ts/lib/function';
import { generateMakeParseCoordinates } from '../../../constructors';
import {
  makeRegexMatchesToCoordinateParts,
  makeRegexParser,
} from '../../../constructors/parseWithRegex';
import { isTupleOfLength } from '../../../utils/lists';
import { negateIf, parseFloatFromParts } from '../../../utils/numbers';
import * as regexUtils from '../../../utils/regex';
import { defaultCharactersets } from '../defaults';
import { isNegativeLatitude, isNegativeLongitude } from '../validations';
import { formatDD } from './constants';
import { coordinatesToLatLonDD } from './convert';
import { validateCoordinatePartsDD, validateCoordinatesDD } from './validate';

export const defaultParserOptionsDD = {
  symbols: {
    north: defaultCharactersets.symbols.north,
    south: defaultCharactersets.symbols.south,
    east: defaultCharactersets.symbols.east,
    west: defaultCharactersets.symbols.west,
    negative: defaultCharactersets.symbols.negative,
    degrees: defaultCharactersets.symbols.degrees,
    whitespace: defaultCharactersets.symbols.whitespace,
  },
  separators: {
    number: defaultCharactersets.separators.number,
    decimal: defaultCharactersets.separators.decimal,
    coordinates: defaultCharactersets.separators.coordinates,
  },
};

export type ParserOptionsDD = typeof defaultParserOptionsDD;

export const matchesToCoordinatePartsDD = makeRegexMatchesToCoordinateParts(
  flow(
    regexUtils.matchesToArray,
    O.fromPredicate(isTupleOfLength(10)),
    O.map(
      ([
        _,
        prefix,
        latDegreesInt,
        latDegreesBetween,
        latDegreesDecimal,
        middle,
        lonDegreesInt,
        lonDegreesBetween,
        lonDegreesDecimal,
        suffix,
      ]) => ({
        prefix,
        latDegreesInt,
        latDegreesBetween,
        latDegreesDecimal,
        middle,
        lonDegreesInt,
        lonDegreesBetween,
        lonDegreesDecimal,
        suffix,
      })
    ),
    O.toUndefined
  )
);

export type CoordinatePartsDD = NonNullable<
  ReturnType<typeof matchesToCoordinatePartsDD>
>;

export const makeParseCoordinatesDD = generateMakeParseCoordinates(
  (options: ParserOptionsDD) =>
    makeRegexParser({
      format: formatDD,
      regexes: pipe(
        (latIntDigits: number) => (lonIntDigits: number) => {
          const makeFloatRegex = (withLeadingLongZero: boolean) =>
            regexUtils.createStringFromParts([
              regexUtils.zeroOrMoreNonDigits,
              ...regexUtils.float(
                latIntDigits,
                options.separators.decimal,
                latIntDigits > 1
              ),
              regexUtils.zeroOrMoreNonDigits,
              ...regexUtils.float(
                lonIntDigits,
                options.separators.decimal,
                lonIntDigits > 1,
                withLeadingLongZero
              ),
              regexUtils.zeroOrMoreNonDigits,
            ]);
          const possibleRegexes = [
            regexUtils.createStringFromParts([
              regexUtils.zeroOrMoreNonDigits,
              ...regexUtils.integer(latIntDigits),
              regexUtils.zeroOrMoreNonDigits,
              ...regexUtils.integer(lonIntDigits),
              regexUtils.zeroOrMoreNonDigits,
            ]),
            makeFloatRegex(false),
          ];
          if (lonIntDigits < 3) possibleRegexes.push(makeFloatRegex(true));
          return possibleRegexes;
        },
        (fn) => pipe([1, 2, 3], A.map(fn)),
        A.ap([1, 2, 3]),
        A.flatten
      ),
      matchesToCoordinates: (matches) =>
        pipe(
          O.Do,
          O.bind('matches', () => O.some(matches)),
          O.bind('parts', ({ matches }) =>
            O.fromNullable(matchesToCoordinatePartsDD(matches))
          ),
          O.bind('coordinates', ({ parts }) =>
            O.some({
              latitude: {
                degrees: pipe(
                  parseFloatFromParts(
                    parts.latDegreesInt,
                    parts.latDegreesDecimal
                  ),
                  negateIf(
                    isNegativeLatitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsSouth: options.symbols.south,
                      prefix: parts.prefix,
                      middle: parts.middle,
                    })
                  )
                ),
              },
              longitude: {
                degrees: pipe(
                  parseFloatFromParts(
                    parts.lonDegreesInt,
                    parts.lonDegreesDecimal
                  ),
                  negateIf(
                    isNegativeLongitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsWest: options.symbols.west,
                      middle: parts.middle,
                      suffix: parts.suffix,
                    })
                  )
                ),
              },
            })
          ),
          O.map(({ matches, parts, coordinates }) => ({
            matches,
            parsingErrors: validateCoordinatePartsDD(parts, options),
            validationErrors: validateCoordinatesDD(coordinates),
            latLon: coordinatesToLatLonDD(coordinates),
          })),
          O.toUndefined
        ),
    })
);
