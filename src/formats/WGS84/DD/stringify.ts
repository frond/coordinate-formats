import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { generateMakeCoordinatesToString } from '../../../constructors';
import { CoordinatesDD } from './convert';
import {
  roundToDecimalPlaces,
  toFixedIntegerDigits,
} from '../../../utils/numbers';
import { join } from '../../../utils/lists';

export const defaultStringifyOptionsDD = {
  symbols: {
    degree: '°',
    negative: '-',
    north: 'N',
    south: 'S',
    east: 'E',
    west: 'W',
  },
  separators: {
    coordinates: ',',
  },
  roundToDigits: 3,
  useCardinalDirections: true,
  addLeadingZeros: true,
};

export type StringifyOptionsDD = typeof defaultStringifyOptionsDD;

export const makeCoordinatesToStringDD = generateMakeCoordinatesToString(
  (options: StringifyOptionsDD) =>
    ({ latitude, longitude }: CoordinatesDD) =>
      pipe(
        [latitude, longitude],
        A.zip([
          {
            integerDigitsDegrees: 2,
            symbolPositive: options.symbols.north,
            symbolNegative: options.symbols.south,
          },
          {
            integerDigitsDegrees: 3,
            symbolPositive: options.symbols.east,
            symbolNegative: options.symbols.west,
          },
        ]),
        A.map(
          ([
            { degrees },
            { integerDigitsDegrees, symbolPositive, symbolNegative },
          ]) =>
            pipe(
              [
                options.useCardinalDirections
                  ? degrees < 0
                    ? `${symbolNegative} `
                    : `${symbolPositive} `
                  : degrees < 0
                  ? options.symbols.negative
                  : '',
                pipe(
                  options.roundToDigits
                    ? roundToDecimalPlaces(options.roundToDigits)(degrees)
                    : degrees,
                  Math.abs,
                  toFixedIntegerDigits(integerDigitsDegrees)
                ),
                options.symbols.degree,
              ],
              join('')
            )
        ),
        join(`${options.separators.coordinates} `)
      )
);
