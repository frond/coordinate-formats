import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import { flow, pipe } from 'fp-ts/lib/function';
import { generateMakeParseCoordinates } from '../../../constructors';
import {
  makeRegexMatchesToCoordinateParts,
  makeRegexParser,
} from '../../../constructors/parseWithRegex';
import { isTupleOfLength, join } from '../../../utils/lists';
import { negateIf, parseFloatFromParts } from '../../../utils/numbers';
import * as regexUtils from '../../../utils/regex';
import { defaultCharactersets } from '../defaults';
import { isNegativeLatitude, isNegativeLongitude } from '../validations';
import { formatDM } from './constants';
import { coordinatesToLatLonDM } from './convert';
import { validateCoordinatePartsDM, validateCoordinatesDM } from './validate';

export const defaultParserOptionsDM = {
  symbols: {
    north: defaultCharactersets.symbols.north,
    south: defaultCharactersets.symbols.south,
    east: defaultCharactersets.symbols.east,
    west: defaultCharactersets.symbols.west,
    negative: defaultCharactersets.symbols.negative,
    degrees: defaultCharactersets.symbols.degrees,
    minutes: defaultCharactersets.symbols.minutes,
    whitespace: defaultCharactersets.symbols.whitespace,
  },
  separators: {
    number: defaultCharactersets.separators.number,
    decimal: defaultCharactersets.separators.decimal,
    coordinates: defaultCharactersets.separators.coordinates,
  },
};

export type ParserOptionsDM = typeof defaultParserOptionsDM;

export const matchesToCoordinatePartsDM = makeRegexMatchesToCoordinateParts(
  flow(
    regexUtils.matchesToArray,
    O.fromPredicate(isTupleOfLength(14)),
    O.map(
      ([
        _,
        prefix,
        latDegrees,
        latBetweenDegAndMin,
        latMinutesInt,
        latMinutesBetween,
        latMinutesDecimal,
        middle,
        lonDegrees,
        lonBetweenDegAndMin,
        lonMinutesInt,
        lonMinutesBetween,
        lonMinutesDecimal,
        suffix,
      ]) => ({
        prefix,
        latDegrees,
        latBetweenDegAndMin,
        latMinutesInt,
        latMinutesBetween,
        latMinutesDecimal,
        middle,
        lonDegrees,
        lonBetweenDegAndMin,
        lonMinutesInt,
        lonMinutesBetween,
        lonMinutesDecimal,
        suffix,
      })
    ),
    O.toUndefined
  )
);

export type CoordinatePartsDM = NonNullable<
  ReturnType<typeof matchesToCoordinatePartsDM>
>;

export const makeParseCoordinatesDM = generateMakeParseCoordinates(
  (options: ParserOptionsDM) =>
    makeRegexParser({
      format: formatDM,
      regexes: pipe(
        (latDigits: number) =>
          (latMinutesDigits: number) =>
          (lonDigits: number) =>
          (lonMinutesDigits: number) =>
            [
              regexUtils.createStringFromParts([
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(latDigits),
                latDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.integer(latMinutesDigits),
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(lonDigits),
                lonDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.integer(lonMinutesDigits),
                regexUtils.zeroOrMoreNonDigits,
              ]),
              regexUtils.createStringFromParts([
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(latDigits),
                latDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.float(
                  latMinutesDigits,
                  options.separators.decimal,
                  latMinutesDigits > 1
                ),
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(lonDigits),
                lonDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.float(
                  lonMinutesDigits,
                  options.separators.decimal,
                  lonMinutesDigits > 1
                ),
                regexUtils.zeroOrMoreNonDigits,
              ]),
            ],
        (fn) => pipe([1, 2, 3], A.map(fn)),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.flatten
      ),
      matchesToCoordinates: (matches) =>
        pipe(
          O.Do,
          O.bind('matches', () => O.some(matches)),
          O.bind('parts', ({ matches }) =>
            O.fromNullable(matchesToCoordinatePartsDM(matches))
          ),
          O.bind('coordinates', ({ parts }) =>
            O.some({
              latitude: {
                degrees: pipe(
                  parts.latDegrees,
                  parseInt,
                  negateIf(
                    isNegativeLatitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsSouth: options.symbols.south,
                      prefix: parts.prefix,
                      middle: parts.middle,
                    })
                  )
                ),
                minutes: parseFloatFromParts(
                  parts.latMinutesInt,
                  parts.latMinutesDecimal
                ),
              },
              longitude: {
                degrees: pipe(
                  parts.lonDegrees,
                  parseInt,
                  negateIf(
                    isNegativeLongitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsWest: options.symbols.west,
                      middle: parts.middle,
                      suffix: parts.suffix,
                    })
                  )
                ),
                minutes: parseFloatFromParts(
                  parts.lonMinutesInt,
                  parts.lonMinutesDecimal
                ),
              },
            })
          ),
          O.map(({ matches, parts, coordinates }) => ({
            matches,
            parsingErrors: validateCoordinatePartsDM(parts, options),
            validationErrors: validateCoordinatesDM(coordinates),
            latLon: coordinatesToLatLonDM(coordinates),
          })),
          O.toUndefined
        ),
    })
);
