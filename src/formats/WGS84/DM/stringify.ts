import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { generateMakeCoordinatesToString } from '../../../constructors';
import { CoordinatesDM } from './convert';
import {
  roundToDecimalPlaces,
  toFixedIntegerDigits,
} from '../../../utils/numbers';
import { join } from '../../../utils/lists';

export const defaultStringifyOptionsDM = {
  symbols: {
    degree: '°',
    minutes: `'`,
    negative: '-',
    north: 'N',
    south: 'S',
    east: 'E',
    west: 'W',
  },
  separators: {
    coordinates: ',',
  },
  roundToDigits: 3,
  useCardinalDirections: true,
  addLeadingZeros: true,
};

export type StringifyOptionsDM = typeof defaultStringifyOptionsDM;

export const makeCoordinatesToStringDM = generateMakeCoordinatesToString(
  (options: StringifyOptionsDM) =>
    ({ latitude, longitude }: CoordinatesDM) =>
      pipe(
        [latitude, longitude],
        A.zip([
          {
            integerDigitsDegrees: 2,
            symbolPositive: options.symbols.north,
            symbolNegative: options.symbols.south,
          },
          {
            integerDigitsDegrees: 3,
            symbolPositive: options.symbols.east,
            symbolNegative: options.symbols.west,
          },
        ]),
        A.map(
          ([
            { degrees, minutes },
            { integerDigitsDegrees, symbolPositive, symbolNegative },
          ]) =>
            pipe(
              [
                options.useCardinalDirections
                  ? degrees < 0
                    ? `${symbolNegative} `
                    : `${symbolPositive} `
                  : degrees < 0
                  ? options.symbols.negative
                  : '',
                pipe(
                  degrees,
                  Math.abs,
                  toFixedIntegerDigits(integerDigitsDegrees)
                ),
                options.symbols.degree,
                ' ',
                pipe(
                  options.roundToDigits
                    ? roundToDecimalPlaces(options.roundToDigits)(minutes)
                    : minutes,
                  Math.abs,
                  toFixedIntegerDigits(2)
                ),
                options.symbols.minutes,
              ],
              join('')
            )
        ),
        join(`${options.separators.coordinates} `)
      )
);
