import { makeCoordinatesFormat } from '../../../constructors';
import { formatDM } from './constants';
import { coordinatesToLatLonDM, latLonToCoordinatesDM } from './convert';
import { defaultParserOptionsDM, makeParseCoordinatesDM } from './parse';
import {
  defaultStringifyOptionsDM,
  makeCoordinatesToStringDM,
} from './stringify';
import { validateCoordinatesDM } from './validate';

/**
 * @category Core
 */
export const DM = makeCoordinatesFormat({
  format: formatDM,
  coordinatesToLatLon: coordinatesToLatLonDM,
  latLonToCoordinates: latLonToCoordinatesDM,
  defaultStringifyOptions: defaultStringifyOptionsDM,
  makeCoordinatesToString: makeCoordinatesToStringDM,
  validateCoordinates: validateCoordinatesDM,
  defaultParserOptions: defaultParserOptionsDM,
  makeParseCoordinates: makeParseCoordinatesDM,
});
