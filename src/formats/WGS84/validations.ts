import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as I from 'fp-ts/lib/Identity';
import { makeValidation } from '../../constructors/validate';

/**
 * @category Validation
 */
export const validateDegrees = makeValidation(
  ({ key, degrees }: { key: string; degrees: number }) =>
    pipe(
      degrees,
      Math.abs,
      (y) => y >= 0 && y <= (key === 'latitude' ? 90 : 180)
    ),
  ({ key }) =>
    `${key} has invalid degrees (should be between 0 and ${
      key === 'latitude' ? 90 : 180
    })`
);

/**
 * @category Validation
 */
export const validateMinutes = makeValidation(
  ({ minutes }: { key: string; minutes: number }) =>
    pipe(minutes, Math.abs, (a) => a >= 0 && a <= 60),
  ({ key }) => `${key} has invalid minutes (should be between 0 and 60)`
);

/**
 * @category Validation
 */
export const validateSeconds = makeValidation(
  ({ seconds }: { key: string; seconds: number }) =>
    pipe(seconds, Math.abs, (a) => a >= 0 && a <= 60),
  ({ key }) => `${key} has invalid seconds (should be between 0 and 60)`
);

/**
 * @category Validation
 */
export const isNegativeCoordinate = ({
  left,
  right,
}: {
  left: {
    symbols: Array<string>;
    match: string;
  };
  right: {
    symbols: Array<string>;
    match: string;
  };
}) =>
  pipe(
    [left, right],
    A.map((x) =>
      pipe(
        x.symbols,
        A.map((a) => x.match.includes(a)),
        A.some(I.of)
      )
    ),
    A.some(I.of)
  );

/**
 * @category Validation
 */
export const isNegativeLatitude = ({
  symbolsNegative,
  symbolsSouth,
  prefix,
  middle,
}: {
  symbolsNegative: Array<string>;
  symbolsSouth: Array<string>;
  prefix: string;
  middle: string;
}) =>
  isNegativeCoordinate({
    left: {
      symbols: [...symbolsSouth, ...symbolsNegative],
      match: prefix,
    },
    right: {
      symbols: [...symbolsSouth],
      match: middle,
    },
  });

/**
 * @category Validation
 */
export const isNegativeLongitude = ({
  symbolsNegative,
  symbolsWest,
  middle,
  suffix,
}: {
  symbolsNegative: Array<string>;
  symbolsWest: Array<string>;
  middle: string;
  suffix: string;
}) =>
  isNegativeCoordinate({
    left: {
      symbols: [...symbolsWest, ...symbolsNegative],
      match: middle,
    },
    right: {
      symbols: [...symbolsWest],
      match: suffix,
    },
  });
