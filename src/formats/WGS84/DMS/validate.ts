import * as A from 'fp-ts/lib/Array';
import { pipe } from 'fp-ts/lib/function';
import * as O from 'fp-ts/lib/Option';
import { makeValidateCoordinates } from '../../../constructors';
import {
  containCharactersConsistently,
  containsOnlyValidCharacters,
} from '../../../validations';
import {
  validateDegrees,
  validateMinutes,
  validateSeconds,
} from '../validations';
import { CoordinatesDMS } from './convert';
import { CoordinatePartsDMS, ParserOptionsDMS } from './parse';

export const validateCoordinatePartsDMS = (
  parts: CoordinatePartsDMS,
  options: ParserOptionsDMS
) =>
  pipe(
    [
      containsOnlyValidCharacters({
        key: 'prefix',
        str: parts.prefix,
        validCharacters: [
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'latitude (between degrees and minutes)',
        str: parts.latBetweenDegAndMin,
        validCharacters: [
          ...options.symbols.degrees,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'latitude (between minutes and seconds)',
        str: parts.latBetweenMinAndSec,
        validCharacters: [
          ...options.symbols.minutes,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'middle',
        str: parts.middle,
        validCharacters: [
          ...options.symbols.seconds,
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.north,
          ...options.symbols.south,
          ...options.symbols.negative,
          ...options.separators.coordinates,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'longitude (between degrees and minutes)',
        str: parts.latBetweenDegAndMin,
        validCharacters: [
          ...options.symbols.degrees,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'longitude (between minutes and seconds)',
        str: parts.lonBetweenMinAndSec,
        validCharacters: [
          ...options.symbols.minutes,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containsOnlyValidCharacters({
        key: 'suffix',
        str: parts.suffix,
        validCharacters: [
          ...options.symbols.east,
          ...options.symbols.west,
          ...options.symbols.seconds,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latDegrees, parts.lonDegrees],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latBetweenDegAndMin, parts.lonBetweenDegAndMin],
        characters: [
          ...options.symbols.degrees,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latMinutes, parts.lonMinutes],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latBetweenMinAndSec, parts.lonBetweenMinAndSec],
        characters: [
          ...options.symbols.minutes,
          ...options.separators.number,
          ...options.symbols.whitespace,
        ],
      }),
      containCharactersConsistently({
        targets: [parts.latSecondsBetween, parts.lonSecondsBetween],
        characters: [
          ...options.separators.decimal,
          ...options.symbols.whitespace,
        ],
      }),
    ],
    A.filterMap(O.fromNullable)
  );

export const validateCoordinatesDMS = makeValidateCoordinates(
  ({ latitude, longitude }: CoordinatesDMS) =>
    pipe(
      [
        { key: 'latitude', coordinate: latitude },
        { key: 'longitude', coordinate: longitude },
      ],
      A.chain(({ key, coordinate }) => [
        validateDegrees({ key, degrees: coordinate.degrees }),
        validateMinutes({ key, minutes: coordinate.minutes }),
        validateSeconds({ key, seconds: coordinate.seconds }),
      ]),
      A.filterMap(O.fromNullable)
    )
);
