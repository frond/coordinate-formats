import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as I from 'fp-ts/lib/Identity';
import * as R from 'fp-ts/lib/Record';
import {
  makeCoordinatesToLatLon,
  makeLatLonToCoordinates,
} from '../../../constructors';
import {
  decimalToHexagecimal,
  hexagecimalToDecimal,
  negateIf,
  subtract,
  numberToString,
} from '../../../utils/numbers';
import { LatLonTuple } from '../../../types';

export const decimalToCoordinateDMS = (x: number) =>
  pipe(
    I.Do,
    I.bind('degreesInt', () => pipe(x, numberToString, parseInt)),
    I.bind('minutes', ({ degreesInt }) =>
      pipe(x, subtract(degreesInt), decimalToHexagecimal)
    ),
    I.bind('minutesInt', ({ minutes }) =>
      pipe(minutes, numberToString, parseInt)
    ),
    I.bind('seconds', ({ minutes, minutesInt }) =>
      pipe(minutes, subtract(minutesInt), decimalToHexagecimal)
    ),
    ({ degreesInt, minutesInt, seconds }) => ({
      degrees: degreesInt,
      minutes: seconds > 59.999 ? minutesInt + 1 : minutesInt,
      seconds: seconds > 59.999 ? 0 : seconds,
    })
  );

export const latLonToCoordinatesDMS = makeLatLonToCoordinates((x) =>
  pipe(
    {
      latitude: x[0],
      longitude: x[1],
    },
    R.map(decimalToCoordinateDMS)
  )
);

export type CoordinatesDMS = ReturnType<typeof latLonToCoordinatesDMS>;

export const coordinatesToLatLonDMS = makeCoordinatesToLatLon(
  ({ latitude, longitude }: CoordinatesDMS) =>
    pipe(
      [latitude, longitude],
      A.map(({ degrees, minutes, seconds }) =>
        pipe(
          Math.abs(degrees) +
            hexagecimalToDecimal(minutes + hexagecimalToDecimal(seconds)),
          negateIf(degrees < 0)
        )
      ),
      (x) => x as LatLonTuple
    )
);
