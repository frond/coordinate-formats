import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import { flow, pipe } from 'fp-ts/lib/function';
import { generateMakeParseCoordinates } from '../../../constructors';
import {
  makeRegexMatchesToCoordinateParts,
  makeRegexParser,
} from '../../../constructors/parseWithRegex';
import { isTupleOfLength, join } from '../../../utils/lists';
import { negateIf, parseFloatFromParts } from '../../../utils/numbers';
import * as regexUtils from '../../../utils/regex';
import { defaultCharactersets } from '../defaults';
import { isNegativeLatitude, isNegativeLongitude } from '../validations';
import { formatDMS } from './constants';
import { coordinatesToLatLonDMS } from './convert';
import { validateCoordinatePartsDMS, validateCoordinatesDMS } from './validate';

export const defaultParserOptionsDMS = {
  symbols: {
    north: defaultCharactersets.symbols.north,
    south: defaultCharactersets.symbols.south,
    east: defaultCharactersets.symbols.east,
    west: defaultCharactersets.symbols.west,
    negative: defaultCharactersets.symbols.negative,
    degrees: defaultCharactersets.symbols.degrees,
    minutes: defaultCharactersets.symbols.minutes,
    seconds: defaultCharactersets.symbols.seconds,
    whitespace: defaultCharactersets.symbols.whitespace,
  },
  separators: {
    number: defaultCharactersets.separators.number,
    decimal: defaultCharactersets.separators.decimal,
    coordinates: defaultCharactersets.separators.coordinates,
  },
};

export type ParserOptionsDMS = typeof defaultParserOptionsDMS;

export const matchesToCoordinatePartsDMS = makeRegexMatchesToCoordinateParts(
  flow(
    regexUtils.matchesToArray,
    O.fromPredicate(isTupleOfLength(18)),
    O.map(
      ([
        _,
        prefix,
        latDegrees,
        latBetweenDegAndMin,
        latMinutes,
        latBetweenMinAndSec,
        latSecondsInt,
        latSecondsBetween,
        latSecondsDecimal,
        middle,
        lonDegrees,
        lonBetweenDegAndMin,
        lonMinutes,
        lonBetweenMinAndSec,
        lonSecondsInt,
        lonSecondsBetween,
        lonSecondsDecimal,
        suffix,
      ]) => ({
        prefix,
        latDegrees,
        latBetweenDegAndMin,
        latMinutes,
        latBetweenMinAndSec,
        latSecondsInt,
        latSecondsBetween,
        latSecondsDecimal,
        middle,
        lonDegrees,
        lonBetweenDegAndMin,
        lonMinutes,
        lonBetweenMinAndSec,
        lonSecondsInt,
        lonSecondsBetween,
        lonSecondsDecimal,
        suffix,
      })
    ),
    O.toUndefined
  )
);

export type CoordinatePartsDMS = NonNullable<
  ReturnType<typeof matchesToCoordinatePartsDMS>
>;

export const makeParseCoordinatesDMS = generateMakeParseCoordinates(
  (options: ParserOptionsDMS) =>
    makeRegexParser({
      format: formatDMS,
      regexes: pipe(
        (latDigits: number) =>
          (latMinutesDigits: number) =>
          (latSecondsDigits: number) =>
          (lonDigits: number) =>
          (lonMinutesDigits: number) =>
          (lonSecondsDigits: number) =>
            [
              regexUtils.createStringFromParts([
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(latDigits),
                latDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                regexUtils.digits(latMinutesDigits),
                latMinutesDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.integer(latSecondsDigits),
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(lonDigits),
                lonDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                regexUtils.digits(lonMinutesDigits),
                lonMinutesDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.integer(lonSecondsDigits),
                regexUtils.zeroOrMoreNonDigits,
              ]),
              regexUtils.createStringFromParts([
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(latDigits),
                latDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                regexUtils.digits(latMinutesDigits),
                latMinutesDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.float(
                  latSecondsDigits,
                  options.separators.decimal,
                  lonSecondsDigits > 1
                ),
                regexUtils.zeroOrMoreNonDigits,
                regexUtils.digits(lonDigits),
                lonDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                regexUtils.digits(lonMinutesDigits),
                lonMinutesDigits > 1
                  ? regexUtils.zeroOrMoreNonDigits
                  : regexUtils.oneOrMoreNonDigits,
                ...regexUtils.float(
                  lonSecondsDigits,
                  options.separators.decimal,
                  lonSecondsDigits > 1
                ),
                regexUtils.zeroOrMoreNonDigits,
              ]),
            ],
        (fn) => pipe([1, 2, 3], A.map(fn)),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.ap([1, 2, 3]),
        A.flatten
      ),
      matchesToCoordinates: (matches) =>
        pipe(
          O.Do,
          O.bind('matches', () => O.some(matches)),
          O.bind('parts', ({ matches }) =>
            O.fromNullable(matchesToCoordinatePartsDMS(matches))
          ),
          O.bind('coordinates', ({ parts }) =>
            O.some({
              latitude: {
                degrees: pipe(
                  parts.latDegrees,
                  parseInt,
                  negateIf(
                    isNegativeLatitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsSouth: options.symbols.south,
                      prefix: parts.prefix,
                      middle: parts.middle,
                    })
                  )
                ),
                minutes: pipe(parts.latMinutes, parseInt),
                seconds: parseFloatFromParts(
                  parts.latSecondsInt,
                  parts.latSecondsDecimal
                ),
              },
              longitude: {
                degrees: pipe(
                  parts.lonDegrees,
                  parseInt,
                  negateIf(
                    isNegativeLongitude({
                      symbolsNegative: options.symbols.negative,
                      symbolsWest: options.symbols.west,
                      middle: parts.middle,
                      suffix: parts.suffix,
                    })
                  )
                ),
                minutes: pipe(parts.lonMinutes, parseInt),
                seconds: parseFloatFromParts(
                  parts.lonSecondsInt,
                  parts.lonSecondsDecimal
                ),
              },
            })
          ),
          O.map(({ matches, parts, coordinates }) => ({
            matches,
            parsingErrors: validateCoordinatePartsDMS(parts, options),
            validationErrors: validateCoordinatesDMS(coordinates),
            latLon: coordinatesToLatLonDMS(coordinates),
          })),
          O.toUndefined
        ),
    })
);
