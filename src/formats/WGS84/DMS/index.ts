import { makeCoordinatesFormat } from '../../../constructors';
import { formatDMS } from './constants';
import { coordinatesToLatLonDMS, latLonToCoordinatesDMS } from './convert';
import { defaultParserOptionsDMS, makeParseCoordinatesDMS } from './parse';
import {
  defaultStringifyOptionsDMS,
  makeCoordinatesToStringDMS,
} from './stringify';
import { validateCoordinatesDMS } from './validate';

/**
 * @category Core
 */
export const DMS = makeCoordinatesFormat({
  format: formatDMS,
  coordinatesToLatLon: coordinatesToLatLonDMS,
  latLonToCoordinates: latLonToCoordinatesDMS,
  defaultStringifyOptions: defaultStringifyOptionsDMS,
  makeCoordinatesToString: makeCoordinatesToStringDMS,
  validateCoordinates: validateCoordinatesDMS,
  defaultParserOptions: defaultParserOptionsDMS,
  makeParseCoordinates: makeParseCoordinatesDMS,
});
