import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { isCoordinatesParserSuccess } from '../constructors';
import { coordinatesParserSuccessToString } from './index';
import { parseCoordinates } from './defaults';
import { fixtures } from './fixtures';

describe('formats', () => {
  describe('parseWithDefaults()', () => {
    fixtures.forEach(({ query, expected }) => {
      const results = pipe(
        parseCoordinates(query),
        A.filter(isCoordinatesParserSuccess),
        A.map(coordinatesParserSuccessToString)
      );
      it(`parses ${query}`, () => {
        expect(results).toEqual(expected);
      });
    });
  });
});
