import OpenCodeLocation from 'open-location-code-typescript';
import { flow, pipe } from 'fp-ts/lib/function';
import * as E from 'fp-ts/lib/Either';
import * as A from 'fp-ts/lib/Array';
import * as I from 'fp-ts/lib/Identity';
import {
  generateMakeParseCoordinates,
  makeCoordinatesFormat,
  makeCoordinatesParserError,
  makeCoordinatesParserSuccess,
} from '../../constructors';
import {
  makeCoordinatesToLatLon,
  makeLatLonToCoordinates,
} from '../../constructors';
import { formatOlc } from './constants';

export const latLonToCoordinatesOlc = makeLatLonToCoordinates((x) =>
  OpenCodeLocation.encode(x[0], x[1])
);

export type CoordinatesOlc = ReturnType<typeof latLonToCoordinatesOlc>;

export const olcToLatLon = makeCoordinatesToLatLon((olc: CoordinatesOlc) =>
  pipe(olc, OpenCodeLocation.decode, (x) => [
    x.latitudeCenter,
    x.longitudeCenter,
  ])
);

export const validateOlc = (str: string) =>
  pipe(
    E.tryCatch(
      () => OpenCodeLocation.decode(str),
      (e) => (e instanceof Error ? e.message : 'unknown error')
    ),
    E.map(() => undefined),
    E.toUnion,
    (x) => [x],
    A.filter((x): x is string => x !== undefined)
  );

export const parseOlc = generateMakeParseCoordinates(
  () => (str) =>
    pipe(
      I.Do,
      I.bind('parsingErrors', () => validateOlc(str)),
      ({ parsingErrors }) =>
        parsingErrors.length
          ? makeCoordinatesParserError({
              format: formatOlc,
              meta: {},
              errors: parsingErrors,
            })
          : makeCoordinatesParserSuccess({
              format: formatOlc,
              latLon: olcToLatLon(str),
              meta: {},
            }),
      (x) => [x]
    )
);

export const OLC = makeCoordinatesFormat({
  format: formatOlc,
  coordinatesToLatLon: olcToLatLon,
  latLonToCoordinates: latLonToCoordinatesOlc,
  defaultStringifyOptions: {},
  makeCoordinatesToString: () => flow(olcToLatLon, latLonToCoordinatesOlc),
  validateCoordinates: validateOlc,
  defaultParserOptions: {},
  makeParseCoordinates: parseOlc,
});
