import * as A from 'fp-ts/lib/Array';
import { pipe } from 'fp-ts/lib/function';
import * as O from 'fp-ts/lib/Option';
import { combineParsers } from '../constructors';
import { CoordinatesParserSuccess } from '../constructors/parse';
import { DD, DM, DMS } from './WGS84';
import { OLC } from './OLC';
import { UrlGoogleMaps, UrlOpenStreetMap } from './URL';
import { ArrayElement } from '../types';

/**
 * @category Core
 */
export const defaultFormats = [
  DD,
  DM,
  DMS,
  OLC,
  UrlOpenStreetMap,
  UrlGoogleMaps,
];

/**
 * @category Core
 */
export const defaultFormatKeys = defaultFormats.map((x) => x.format);

/**
 * @category Core
 */
export type DefaultFormatKey = ArrayElement<typeof defaultFormatKeys>;

/**
 * @category Core
 */
export const parseCoordinates = combineParsers(
  pipe(
    defaultFormats,
    A.map((x) => x.parseCoordinates)
  )
);

/**
 * @category Conversion
 */
export const coordinatesParserSuccessToString = <K extends string>(
  x: CoordinatesParserSuccess<K>
) =>
  pipe(
    x.latLon,
    pipe(
      defaultFormats,
      A.findFirst((a) => a.format === x.format),
      O.fold(
        () => DD.latLonToString,
        (a) => a.latLonToString
      )
    )
  );
