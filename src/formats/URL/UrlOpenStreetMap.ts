import { flow, pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import {
  generateMakeParseCoordinates,
  makeCoordinatesFormat,
  makeCoordinatesParserError,
  makeCoordinatesParserSuccess,
} from '../../constructors';
import { formatUrlOpenStreetMap } from './constants';
import { LatLonTuple } from '../../types';
import { matchesRegex } from '../../utils/regex';
import { split } from 'fp-ts/lib/string';
import { DD } from '../WGS84';

export const urlOpenStreetMapToLatLon = (str: string) =>
  pipe(
    str,
    split('#map='),
    (x) => x[1] as string,
    split('/'),
    (x) => [x[1], x[2]] as [string, string],
    A.map((x) => parseFloat(x)),
    (x) => x as LatLonTuple
  );

export type CoordinatesUrlOpenStreetMap = LatLonTuple;

export const latLonToOpenStreetMapUrl = (latLon: LatLonTuple) =>
  `https://www.openstreetmap.org/#map=11/${latLon[0]}/${latLon[1]}`;

export const validateUrlOpenStreetMap = (str: string) =>
  pipe(
    '.*openstreetmap\\.org.+#map=-?\\d{1,2}\\/\\d+\\.\\d+\\/-?\\d+\\.\\d+',
    matchesRegex(str),
    (isValid) => (isValid ? [] : ['Could not parse OpenStreetMap Url.'])
  );

export const parseUrlOpenStreetMap = generateMakeParseCoordinates(
  () => (str: string) =>
    pipe(
      str,
      validateUrlOpenStreetMap,
      (errorMessages) =>
        errorMessages.length
          ? makeCoordinatesParserError({
              format: formatUrlOpenStreetMap,
              meta: {
                str,
              },
              errors: errorMessages,
            })
          : makeCoordinatesParserSuccess({
              format: formatUrlOpenStreetMap,
              latLon: urlOpenStreetMapToLatLon(str),
              meta: {},
            }),
      (x) => [x]
    )
);

export const UrlOpenStreetMap = makeCoordinatesFormat({
  format: formatUrlOpenStreetMap,
  coordinatesToLatLon: urlOpenStreetMapToLatLon,
  latLonToCoordinates: latLonToOpenStreetMapUrl,
  defaultStringifyOptions: {},
  makeCoordinatesToString: () =>
    flow(urlOpenStreetMapToLatLon, DD.latLonToString),
  validateCoordinates: validateUrlOpenStreetMap,
  defaultParserOptions: {},
  makeParseCoordinates: parseUrlOpenStreetMap,
});
