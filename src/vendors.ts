import * as L from 'leaflet';
import { LngLat } from 'mapbox-gl';
import { LatLonTuple } from './types';

/**
 * @category Vendor Utility
 */
export const leafletLatLngToTuple = (x: L.LatLng): LatLonTuple => [
  x.lat,
  x.lng,
];

/**
 * @category Vendor Utility
 */
export const mapboxglLngLatToLatLonTuple = (x: LngLat): LatLonTuple => [
  x.lat,
  x.lng,
];
