import * as A from 'fp-ts/lib/Array';
import { flow, pipe } from 'fp-ts/lib/function';
import { LatLonTuple } from '../types';
import { leafletLatLngToTuple, mapboxglLngLatToLatLonTuple } from '../vendors';
import {
  CoordinatesParserError,
  CoordinatesParserResult,
  CoordinatesParserSuccess,
  CoordinatesParserValidationError,
  isCoordinatesParserError,
  isCoordinatesParserSuccess,
  isCoordinatesParserValidationError,
} from './parse';

/**
 * @category Constructor
 */
export const makeCoordinatesToLatLon = <T>(fn: (x: T) => LatLonTuple) => fn;

/**
 * @category Signature
 */
export type CoordinatesToLatLon<T> = ReturnType<
  typeof makeCoordinatesToLatLon<T>
>;

/**
 * @category Constructor
 */
export const makeLatLonToCoordinates = <T>(fn: (x: LatLonTuple) => T) => fn;

/**
 * @category Signature
 */
export type LatLonToCoordinates<T> = ReturnType<
  typeof makeLatLonToCoordinates<T>
>;

/**
 * @category Signature
 */
export type CoordinatesToString<T> = (x: T) => string;

/**
 * @category Constructor
 */
export const generateMakeCoordinatesToString = <T, SO>(
  fn: (options: SO) => CoordinatesToString<T>
) => fn;

/**
 * @category Signature
 */
export type MakeCoordinatesToString<T, SO> = ReturnType<
  typeof generateMakeCoordinatesToString<T, SO>
>;

/**
 * @category Constructor
 */
export const makeValidateCoordinates = <T>(fn: (x: T) => Array<string>) => fn;

/**
 * @category Signature
 */
export type ValidateCoordinates<T> = ReturnType<
  typeof makeValidateCoordinates<T>
>;

/**
 * @category Signature
 */
export type ParseCoordinates<K extends string> = (
  str: string
) => Array<CoordinatesParserResult<K>>;

/**
 * @category Constructor
 */
export const generateMakeParseCoordinates = <K extends string, PO>(
  fn: (options: PO) => ParseCoordinates<K>
) => fn;

/**
 * @category Signature
 */
export type MakeParseCoordinates<K extends string, PO> = ReturnType<
  typeof generateMakeParseCoordinates<K, PO>
>;

/**
 * @category Constructor
 */
export const makeCoordinatesFormat = <K extends string, T, PO, SO>(spec: {
  format: K;
  coordinatesToLatLon: CoordinatesToLatLon<T>;
  latLonToCoordinates: LatLonToCoordinates<T>;
  defaultStringifyOptions: SO;
  makeCoordinatesToString: MakeCoordinatesToString<T, SO>;
  validateCoordinates: ValidateCoordinates<T>;
  defaultParserOptions: PO;
  makeParseCoordinates: MakeParseCoordinates<K, PO>;
}) => {
  const coordinatesToString = spec.makeCoordinatesToString(
    spec.defaultStringifyOptions
  );

  const parseCoordinates = spec.makeParseCoordinates(spec.defaultParserOptions);

  function isFormatOfResult(
    x: CoordinatesParserError
  ): x is CoordinatesParserError<K>;
  function isFormatOfResult(
    x: CoordinatesParserValidationError
  ): x is CoordinatesParserValidationError<K>;
  function isFormatOfResult(
    x: CoordinatesParserSuccess
  ): x is CoordinatesParserSuccess<K>;
  function isFormatOfResult(
    x: CoordinatesParserResult
  ): x is CoordinatesParserResult<K> {
    return x.format === spec.format;
  }

  const isParserError = (
    x: CoordinatesParserResult
  ): x is CoordinatesParserError<K> =>
    isCoordinatesParserError(x) && isFormatOfResult(x);

  const isParserValidationError = (
    x: CoordinatesParserResult
  ): x is CoordinatesParserValidationError<K> =>
    isCoordinatesParserValidationError(x) && isFormatOfResult(x);

  const isParserSuccess = (
    x: CoordinatesParserResult
  ): x is CoordinatesParserSuccess<K> =>
    isCoordinatesParserSuccess(x) && isFormatOfResult(x);

  const makeLatLonToString = (options: SO) =>
    flow(spec.latLonToCoordinates, spec.makeCoordinatesToString(options));

  const latLonToString = makeLatLonToString(spec.defaultStringifyOptions);

  const leafletLatLngToCoordinates = flow(
    leafletLatLngToTuple,
    spec.latLonToCoordinates
  );

  const makeLeafletLatLngToString = (options: SO) =>
    flow(leafletLatLngToCoordinates, spec.makeCoordinatesToString(options));

  const leafletLatLngToString = makeLeafletLatLngToString(
    spec.defaultStringifyOptions
  );

  const mapboxglLngLatToCoordinates = flow(
    mapboxglLngLatToLatLonTuple,
    spec.latLonToCoordinates
  );

  const makeMapboxglLngLatToString = (options: SO) =>
    flow(mapboxglLngLatToCoordinates, spec.makeCoordinatesToString(options));

  const mapboxglLngLatToString = makeMapboxglLngLatToString(
    spec.defaultStringifyOptions
  );

  return {
    ...spec,
    coordinatesToString,
    parseCoordinates,
    isParserError,
    isParserValidationError,
    isParserSuccess,
    isFormatOfResult,
    makeLatLonToString,
    latLonToString,
    leafletLatLngToCoordinates,
    makeLeafletLatLngToString,
    leafletLatLngToString,
    mapboxglLngLatToCoordinates,
    makeMapboxglLngLatToString,
    mapboxglLngLatToString,
  };
};

/**
 * @category Signature
 */
export type CoordinatesFormat<
  K extends string = string,
  T = unknown,
  PO = unknown,
  SO = unknown
> = ReturnType<typeof makeCoordinatesFormat<K, T, PO, SO>>;

/**
 * @category Utility
 */
export const combineParsers =
  <K extends string>(parsers: Array<ParseCoordinates<K>>) =>
  (str: string) =>
    pipe(
      parsers,
      A.chain((fn) => fn(str))
    );
