import * as Eq from 'fp-ts/lib/Eq';
import { LatLonTuple } from '../types';
import { pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import * as S from 'fp-ts/lib/string';

/**
 * @category Constant
 */
export const PARSER_RESULT_TAGS = {
  ParserError: 'ParserError',
  ParserValidationError: 'ParserValidationError',
  ParserSuccess: 'ParserSuccess',
} as const;

/**
 * @category Constructor
 */
export const makeCoordinatesParserError = <K extends string>(spec: {
  format: K;
  errors: Array<string>;
  meta: Record<string, any>;
}) => ({
  _tag: PARSER_RESULT_TAGS.ParserError,
  ...spec,
});

/**
 * @category Core
 */
export type CoordinatesParserError<K extends string = string> = ReturnType<
  typeof makeCoordinatesParserError<K>
>;

/**
 * @category Constructor
 */
export const makeCoordinatesParserValidationError = <K extends string>(spec: {
  format: K;
  errors: Array<string>;
  latLon: LatLonTuple;
  meta: Record<string, any>;
}) => ({
  _tag: PARSER_RESULT_TAGS.ParserValidationError,
  ...spec,
});

/**
 * @category Core
 */
export type CoordinatesParserValidationError<K extends string = string> =
  ReturnType<typeof makeCoordinatesParserValidationError<K>>;

/**
 * @category Constructor
 */
export const makeCoordinatesParserSuccess = <K extends string>(spec: {
  format: K;
  latLon: LatLonTuple;
  meta: Record<string, any>;
}) => ({
  _tag: PARSER_RESULT_TAGS.ParserSuccess,
  ...spec,
});

/**
 * @category Core
 */
export type CoordinatesParserSuccess<K extends string = string> = ReturnType<
  typeof makeCoordinatesParserSuccess<K>
>;

/**
 * @category Core
 */
export type CoordinatesParserResult<K extends string = string> =
  | CoordinatesParserError<K>
  | CoordinatesParserValidationError<K>
  | CoordinatesParserSuccess<K>;

/**
 * @category Predicate
 */
export const isCoordinatesParserError = <K extends string>(
  x: CoordinatesParserResult<K>
): x is CoordinatesParserError<K> => x._tag === PARSER_RESULT_TAGS.ParserError;

/**
 * @category Predicate
 */
export const isCoordinatesParserValidationError = <K extends string>(
  x: CoordinatesParserResult<K>
): x is CoordinatesParserValidationError<K> =>
  x._tag === PARSER_RESULT_TAGS.ParserValidationError;

/**
 * @category Predicate
 */
export const isCoordinatesParserSuccess = <K extends string>(
  x: CoordinatesParserResult<K>
): x is CoordinatesParserSuccess<K> =>
  x._tag === PARSER_RESULT_TAGS.ParserSuccess;

/**
 * @category Utility
 */
export const equalsCoordinatesParserResult =
  Eq.fromEquals<CoordinatesParserResult>((x, y) => {
    if (Eq.eqStrict.equals(x._tag, y._tag)) {
      if (isCoordinatesParserError(x) && isCoordinatesParserError(y)) {
        return equalsCoordinatesParserError.equals(x, y);
      }
      if (
        isCoordinatesParserValidationError(x) &&
        isCoordinatesParserValidationError(y)
      ) {
        return equalsCoordinatesParserValidationError.equals(x, y);
      }
      if (isCoordinatesParserSuccess(x) && isCoordinatesParserSuccess(y)) {
        return equalsCoordinatesParserSuccess.equals(x, y);
      }
    }
    return false;
  });

/**
 * @category Utility
 */
export const equalsCoordinatesParserError =
  Eq.fromEquals<CoordinatesParserError>(
    (x, y) =>
      Eq.eqStrict.equals(x.format, y.format) &&
      pipe(x.errors, A.difference(S.Eq)(y.errors), A.isEmpty)
  );

/**
 * @category Utility
 */
export const equalsCoordinatesParserValidationError =
  Eq.fromEquals<CoordinatesParserValidationError>(
    (x, y) =>
      Eq.eqStrict.equals(x.format, y.format) &&
      Eq.eqStrict.equals(x.latLon[0], y.latLon[0]) &&
      Eq.eqStrict.equals(x.latLon[1], y.latLon[1]) &&
      pipe(x.errors, A.difference(S.Eq)(y.errors), A.isEmpty)
  );

/**
 * @category Utility
 */
export const equalsCoordinatesParserSuccess =
  Eq.fromEquals<CoordinatesParserSuccess>(
    (x, y) =>
      Eq.eqStrict.equals(x.format, y.format) &&
      Eq.eqStrict.equals(x.latLon[0], y.latLon[0]) &&
      Eq.eqStrict.equals(x.latLon[1], y.latLon[1])
  );
