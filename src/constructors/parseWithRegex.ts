import * as A from 'fp-ts/lib/Array';
import * as E from 'fp-ts/lib/Either';
import { pipe } from 'fp-ts/lib/function';
import { LatLonTuple } from '../types';
import * as regexUtils from '../utils/regex';
import {
  equalsCoordinatesParserResult,
  makeCoordinatesParserError,
  makeCoordinatesParserSuccess,
  makeCoordinatesParserValidationError,
} from './parse';

/**
 * @category Constructor
 */
export const makeRegexMatchesToCoordinateParts = <T>(
  fn: (x: RegExpExecArray) => T
) => fn;

/**
 * @category Constructor
 */
export const makeRegexParser =
  <K extends string>({
    format,
    regexes,
    matchesToCoordinates,
  }: {
    format: K;
    regexes: Array<RegExp>;
    matchesToCoordinates: (x: RegExpExecArray) =>
      | {
          matches: RegExpExecArray;
          parsingErrors: Array<string>;
          validationErrors: Array<string>;
          latLon: LatLonTuple;
        }
      | undefined;
  }) =>
  (str: string) =>
    pipe(
      regexes,
      A.map((regex) =>
        pipe(
          str,
          regexUtils.execute(regex),
          E.fromNullable(
            makeCoordinatesParserError({
              format,
              errors: ['Could not parse the coordinates string.'],
              meta: {
                regex: regex.source,
              },
            })
          ),
          E.chain((matches) =>
            pipe(matches, matchesToCoordinates, (x) =>
              x === undefined
                ? E.left(
                    makeCoordinatesParserError({
                      format,
                      errors: ['Could not parse the coordinates string.'],
                      meta: {
                        regex: regex.source,
                      },
                    })
                  )
                : E.right(x)
            )
          ),
          E.map(({ matches, parsingErrors, validationErrors, latLon }) =>
            parsingErrors.length
              ? makeCoordinatesParserError({
                  format,
                  errors: parsingErrors,
                  meta: {
                    regex: regex.source,
                    matches,
                  },
                })
              : validationErrors.length
              ? makeCoordinatesParserValidationError({
                  format,
                  errors: validationErrors,
                  latLon,
                  meta: {
                    regex: regex.source,
                    matches,
                  },
                })
              : makeCoordinatesParserSuccess({
                  format,
                  latLon,
                  meta: {
                    regex: regex.source,
                    matches,
                  },
                })
          ),
          E.toUnion
        )
      ),
      A.uniq(equalsCoordinatesParserResult)
    );
