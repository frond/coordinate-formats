import * as A from 'fp-ts/lib/Array';
import * as O from 'fp-ts/lib/Option';
import * as I from 'fp-ts/lib/Identity';
import * as S from 'fp-ts/lib/string';

import { flow, pipe } from 'fp-ts/lib/function';
import { not } from 'fp-ts/lib/Predicate';
import { makeValidation } from './constructors/validate';
import { consistsOf } from './utils/regex';

/**
 * @category Validation
 */
export const containsOnlyValidCharacters = makeValidation(
  ({
    str,
    validCharacters,
  }: {
    key: string;
    str: string;
    validCharacters: string[];
  }) =>
    pipe(
      str,
      O.fromPredicate((x) => x.length > 0),
      O.map(flow(consistsOf(validCharacters))),
      O.getOrElse(() => true)
    ),
  ({ key, validCharacters: validCharacters }) =>
    `${key} contains invalid character(s). Valid characters: [${validCharacters.join(
      ','
    )}]`
);

/**
 * @category Validation
 */
export const containCharactersConsistently = makeValidation(
  ({ targets, characters }: { targets: string[]; characters: string[] }) =>
    pipe(
      characters,
      A.map(
        (c) =>
          pipe(targets, A.every(S.includes(c))) ||
          pipe(targets, A.every(not(S.includes(c))))
      ),
      A.every(I.of)
    ),
  ({ targets, characters }) =>
    `targets [${targets.join(
      ', '
    )}] do not contain the charactersets [${characters.join(
      ', '
    )}] consistently`
);
