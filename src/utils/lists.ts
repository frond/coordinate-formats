/**
 * @category Utility
 */
export const join =
  (separator = '') =>
  <T>(x: Array<T>) =>
    x.join(separator);

/**
 * @category Utility
 */
export type SpecifiedLengthTuple<
  T,
  N extends number,
  A extends Array<unknown> = []
> = A['length'] extends N ? A : SpecifiedLengthTuple<T, N, [...A, T]>;

/**
 * @category Utility
 */
export const isTupleOfLength =
  <N extends number>(n: N) =>
  <T>(x: T[]): x is SpecifiedLengthTuple<T, N> =>
    x.length === n;

/**
 * @category Utility
 */
export const makeTuple = <N extends number, T>(n: N, v: T) =>
  Array.from({ length: n }, () => v) as SpecifiedLengthTuple<T, N>;
