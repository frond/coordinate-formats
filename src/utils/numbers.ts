import { flow, pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { not } from 'fp-ts/lib/Predicate';
import { join, makeTuple } from './lists';

/**
 * @category Utility
 */
export const negate = (x: number) => x * -1;

/**
 * @category Utility
 */
export const negateIf = (x: boolean) => (y: number) => x ? negate(y) : y;

/**
 * @category Utility
 */
export const parseFloatFromParts = (int: string, decimal: string) =>
  decimal === ''
    ? pipe(int, parseInt)
    : pipe([int, decimal], join('.'), parseFloat);

/**
 * @category Utility
 */
export const hexagecimalToDecimal = (x: number) => x / 60;

/**
 * @category Utility
 */
export const decimalToHexagecimal = (x: number) => x * 60;

/**
 * @category Utility
 */
export const toFixedIntegerDigits = (digits: number) => (x: number) =>
  pipe(x, numberToString, parseIntDecimal, numberToString, (b) =>
    b.length < digits
      ? pipe(makeTuple(digits - b.length, '0'), A.append(x), join(''))
      : pipe(x, numberToString)
  );

/**
 * @category Utility
 */
export const roundToDecimalPlaces = (decimalPlaces: number) =>
  flow(
    multiply(exponentiate(decimalPlaces)(10)),
    Math.round,
    divide(exponentiate(decimalPlaces)(10))
  );

/**
 * @category Utility
 */
export const subtract = (b: number) => (a: number) => a - b;

/**
 * @category Utility
 */
export const add = (b: number) => (a: number) => a + b;

/**
 * @category Utility
 */
export const exponentiate = (b: number) => (a: number) => a ** b;

/**
 * @category Utility
 */
export const multiply = (b: number) => (a: number) => a * b;

/**
 * @category Utility
 */
export const divide = (b: number) => (a: number) => a / b;

/**
 * @category Utility
 */
export const numberToString = (x: number) => x.toString();

/**
 * @category Utility
 */
export const parseIntWithRadix = (radix: number) => (x: string) =>
  parseInt(x, radix);

/**
 * @category Utility
 */
export const parseIntDecimal = parseIntWithRadix(10);

/**
 * @category Utility
 */
export const isNaN = (x: number) => Number.isNaN(x);

/**
 * @category Utility
 */
export const isIntString = flow(parseIntDecimal, not(isNaN));
