import { pipe } from 'fp-ts/lib/function';
import { consistsOf, matchesRegex } from './regex';

describe('regex', () => {
  describe(matchesRegex, () => {
    it('returns true if the string matches the given regex', () => {
      expect(pipe('.*', matchesRegex('foo'))).toBe(true);
    });
  });
  describe('consistsOf', () => {
    it('returns true if the string consists of the given substrings', () => {
      expect(consistsOf(['foo', 'bar'])('foobar')).toBe(true);
      expect(consistsOf(['foo', 'bar'])('barfoo')).toBe(true);
      expect(consistsOf(['foo', 'bar'])('barfoobar')).toBe(true);
      expect(consistsOf(['foo', 'bar'])('foo')).toBe(true);
    });
    it('returns false if the string does not consist of the given substrings', () => {
      expect(consistsOf(['foo', 'bar'])('foox')).toBe(false);
      expect(consistsOf(['foo'])('barfoobar')).toBe(false);
    });
  });
});
