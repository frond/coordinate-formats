import { flow, pipe } from 'fp-ts/lib/function';
import * as A from 'fp-ts/lib/Array';
import { join } from './lists';
import { append, wrap } from './strings';
import { numberToString } from './numbers';

/**
 * @category Utility
 */
export const create = (str: string) => new RegExp(str);

/**
 * @category Utility
 */
export const execute = (regex: RegExp) => (str: string) => regex.exec(str);

/**
 * @category Utility
 */
export const matchesToArray = (x: RegExpExecArray) => Array.from(x);

/**
 * @category Utility
 */
export const or = join('|');

/**
 * @category Utility
 */
export const nonCapturingGroup = wrap('(?:', ')');

/**
 * @category Utility
 */
export const capturingGroup = wrap('(', ')');

/**
 * @category Utility
 */
export const zeroOrOne = append('?');

/**
 * @category Utility
 */
export const zeroOrMore = append('*');

/**
 * @category Utility
 */
export const oneOrMore = append('+');

/**
 * @category Utility
 */
export const nOf = (n: number) =>
  append(pipe(n, numberToString, wrap('{', '}')));

/**
 * @category Utility
 */
export const nOrMore = (n: number) =>
  append(pipe(n, numberToString, wrap('{', ',}')));

/**
 * @category Utility
 */
export const nOrLess = (n: number) =>
  append(pipe(n, numberToString, wrap('{,', '}')));

/**
 * @category Utility
 */
export const nBetween = (a: number, b: number) =>
  append(pipe([a, b], join(','), wrap('{', '}')));

/**
 * @category Utility
 */
export const escapeChar = (x: string) =>
  ['.', `\\`, '/', '*', '?', '^', '$', '|', '+'].includes(x) ? `\\${x}` : x;

/**
 * @category Utility
 */
export const escapecharacters = A.map(escapeChar);

/**
 * @category Utility
 */
export const characterSet = flow(escapecharacters, join(), wrap('[', ']'));

/**
 * @category Utility
 */
export const DIGIT = '\\d';

/**
 * @category Utility
 */
export const NON_DIGIT = '\\D';

/**
 * @category Utility
 */
export const WHITESPACE = '\\s';

/**
 * @category Utility
 */
export const EMPTY = '';

/**
 * @category Utility
 */
export const fenceString = wrap('^', '$');

/**
 * @category Utility
 */
export const float = (
  digitsInt: number,
  separators: Array<string>,
  hasOptionalSeparator: boolean,
  withLeadingZero = false
) => [
  digits(digitsInt, withLeadingZero),
  pipe(separators, characterSet, hasOptionalSeparator ? zeroOrMore : oneOrMore),
  pipe(DIGIT, oneOrMore),
];

/**
 * @category Utility
 */
export const floatWithOptionalSeparator = (
  digitsInt: number,
  separators: Array<string>
) => [
  digits(digitsInt),
  pipe(separators, characterSet, zeroOrMore),
  pipe(DIGIT, oneOrMore),
];

/**
 * @category Utility
 */
export const integer = (digitsInt: number) => [digits(digitsInt), EMPTY, EMPTY];

/**
 * @category Utility
 */
export const digits = (digits: number, withLeadingZero = false) =>
  `${withLeadingZero ? '0' : ''}${pipe(DIGIT, nOf(digits))}`;

/**
 * @category Utility
 */
export const zeroOrMoreNonDigits = pipe(NON_DIGIT, zeroOrMore);

/**
 * @category Utility
 */
export const oneOrMoreNonDigits = pipe(NON_DIGIT, oneOrMore);

/**
 * @category Utility
 */
export const createStringFromParts = (parts: Array<string>) =>
  pipe(parts, A.map(capturingGroup), join(), fenceString, create);

/**
 * @category Utility
 */
export const matchesRegex = (str: string) =>
  flow(create, execute, (fn) => fn(str) !== null);

/**
 * @category Utility
 */
export const consistsOf = (blocks: Array<string>) => (str: string) =>
  pipe(
    blocks,
    escapecharacters,
    A.map(nonCapturingGroup),
    or,
    nonCapturingGroup,
    oneOrMore,
    fenceString,
    matchesRegex(str)
  );
